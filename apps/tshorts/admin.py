from django.contrib import admin
from models import Clothe, Buy_form_for_clothe, Clothe_name, Clothe_size, ClotheType, City, PrintType
# Register your models here.

admin.site.register(Clothe_size)
admin.site.register(Clothe)
admin.site.register(Clothe_name)
admin.site.register(Buy_form_for_clothe)
admin.site.register(ClotheType)
admin.site.register(City)
admin.site.register(PrintType)