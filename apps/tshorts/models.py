# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class ClotheType(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Clothe_name(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Clothe_size(models.Model):
    size = models.CharField(max_length=200)

    def __unicode__(self):
        return self.size


class Clothe(models.Model):
    name = models.ForeignKey(Clothe_name)
    clothe_type = models.ForeignKey(ClotheType)
    sizes = models.ManyToManyField(Clothe_size)
    clothe_name = models.CharField(max_length=200)
    clothe_price = models.IntegerField()
    front_img = models.ImageField(upload_to='front_img')
    back_img = models.ImageField(upload_to='back_img')
    clothe_info = models.TextField()

    def __unicode__(self):
        return self.clothe_name

class City(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class PrintType(models.Model):
    type_name = models.CharField(max_length=200)
    price = models.IntegerField()

    def __unicode__(self):
        return self.type_name


class Buy_form_for_clothe(models.Model):
    clothe = models.ForeignKey(Clothe)
    print_type = models.ForeignKey(PrintType)
    size = models.ForeignKey(Clothe_size)
    city_name = models.ForeignKey(City, verbose_name='Название города')
    address = models.CharField(max_length=200, verbose_name='Адрес')
    quantity = models.IntegerField(verbose_name='Количество товара')
    phone_number = models.CharField(max_length=200, verbose_name='Номер вашего телефона')
    clothe_name = models.CharField(max_length=200)
    canvas_img_front = models.TextField(blank=True)
    canvas_img_back = models.TextField(blank=True)
    date = models.DateField(default=timezone.now())
    clothe_price = models.IntegerField(verbose_name='Общая сумма')

    def __unicode__(self):
        return self.clothe_name