from django import forms
from models import Buy_form_for_clothe, Clothe_size

class Form_for_buy_clothe(forms.ModelForm):
    class Meta:
        model = Buy_form_for_clothe
        fields = ['clothe_name', 'clothe', 'size', 'canvas_img_front', 'canvas_img_back', 'clothe_price', 'quantity', 'phone_number', 'city_name', 'address', 'print_type']
        