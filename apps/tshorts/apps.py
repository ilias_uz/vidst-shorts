from __future__ import unicode_literals

from django.apps import AppConfig


class TshortsConfig(AppConfig):
    name = 'tshorts'
