from django.shortcuts import render, redirect
from models import Clothe, Buy_form_for_clothe, ClotheType, PrintType
from forms import Form_for_buy_clothe
# Create your views here.

def index(request):
    c_type = ClotheType.objects.all()
    index = Clothe.objects.all()
    return render(request, 'index.html', {'index': index, 'c_type': c_type})


def clothe_editor(request, id):
    clothe = Clothe.objects.get(pk=id)
    types = PrintType.objects.all()
    if request.method == 'POST':
        form = Form_for_buy_clothe(request.POST)
        if form.is_valid():
            form.save(commit=False)
            clothe = Clothe.objects.get(pk=request.POST.get('clothe'))
            form.clothe = clothe
            form.clothe_name = request.POST.get('clothe_name')
            # form.size = request.POST.get('size')
            form.canvas_img_front = request.POST.get('canvas_img_front')
            form.canvas_img_back = request.POST.get('canvas_img_back')
            instance = form.save(commit=False)
            instance.save()
            form.save()
            return redirect('/')
    else:
        form = Form_for_buy_clothe()
    return render(request, 'clothe_editor.html', {'clothe': clothe, 'form': form, 'types': types})


def custom_list(request):
    buy_list = Buy_form_for_clothe.objects.all().order_by('date')
    return render(request, 'custom_list.html', {'buy_list': buy_list})


def admin_panel(request, id):
    buy_form = Buy_form_for_clothe.objects.get(pk=id)
    return render(request, 'admin-panel.html', {'buy_form': buy_form})